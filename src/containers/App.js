import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import CountryList from '../components/CountryList/CountryList';
import CountryInfo from '../components/CountryInfo/CountryInfo';
import Wrapper from '../components/Wrapper/Wrapper'

class App extends Component {
  state = {
    countryList: [],
    countryInfo: null,
    showWrapper :false,
  };

countryListHandler = (country) => {
axios.get('name/' + country).then(response => {
  this.setState({showWrapper : true});
  const countryInfo = response.data[0];
  Promise.all(response.data[0].borders.map((border)=>{
  return axios.get('alpha/' + border).then(response => {
     return response.data.name;
   })
 })).then(response => {
   const borders = response;
   countryInfo.borders = borders;
   this.setState({countryInfo});
   this.setState({showWrapper: false});
 })
})
};

componentDidMount(){
  axios.get('all?fields=name;alpha3Code').then(response => {
    return this.setState({countryList: response.data})
  })
};

  render() {
    return (
      <div className="App">
        <CountryList
          cliked={this.countryListHandler}
          countryList={this.state.countryList}
        />
        <CountryInfo country={this.state.countryInfo}/>
        <Wrapper show={this.state.showWrapper}/>
      </div>
    );
  }
}

export default App;
