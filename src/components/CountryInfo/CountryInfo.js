import React, {Component} from 'react';
import './CountryInfo.css';

class CountryInfo extends Component{
  showBorders = () => {
    if (this.props.country.borders.length !== 0) {
      return <span>граничит с: {this.props.country.borders.map((border, id) =>{
        return <span key={id}>{border}, </span>;
      })}</span>
    } else {
      return <span> Ни с кем не граничит</span>
    };
  }
render(){
  if (this.props.country) {
    return (
      <div className='CountryInfo'>
        <p>Название страны: {this.props.country.name} </p>
        <p>Общая площадь: {this.props.country.area} кв.км</p>
        <p>Флаг страны: <img width='200' height='150' alt={this.props.country.flag} src={this.props.country.flag}/> {this.props.flag}</p>
        <p>{this.showBorders()}</p>
      </div>
    );
  };
  return <div className='CountryInfo'><p>Выбирите страну из списка</p></div>
}
};

export default CountryInfo;
