import React from 'react';
import './CountryList.css';

const CountryList = props => <div className='CountryList'>
  {props.countryList.map((country, index) => {
    return <p onClick={()=> props.cliked(country.name)} key={index} className='countryInList'>{country.name}</p>
  })}
    </div>

export default CountryList;
